function sendRequestViaMultiFriendSelector(url, message) {
    
    
    function requestCallback(response) {
        $.post('/fb_request', { request: response.request, url: url } );
    }
    
    FB.ui({
      method: 'apprequests',
      message: message
    }, requestCallback);
}